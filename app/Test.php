<?php

namespace App;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

use Illuminate\Database\Eloquent\Model;

class Test extends Model implements StaplerableInterface
{
    use EloquentTrait;

    protected $table='test';
    
	// Add the 'avatar' attachment to the fillable array so that it's mass-assignable on this model.
	protected $fillable = ['avatar', 'name'];

	public function __construct(array $attributes = array()) {
		$this->hasAttachedFile('avatar', [
			'styles' => [
				'medium' => '300x300',
				'thumb' => '100x100'
			]
		]);

		parent::__construct($attributes);
	}
}
