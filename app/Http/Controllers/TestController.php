<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request)
    {
        Test::create($request->all());
        return redirect('/');
    }
}
